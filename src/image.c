//
// Created by ���ᨬ on 31.12.2023.
//

#include "image.h"

struct image create_image(uint64_t const width, uint64_t const height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = (struct pixel*) malloc(sizeof(struct pixel) * width * height)
    };
}

struct image copy_image(struct image * const image) {
    struct image new_image = create_image(image->width, image->height);
    for (uint64_t i = 0; i < new_image.width * new_image.height; ++i) {
        new_image.data[i] = image->data[i];
    }
    return new_image;
}

void delete_image(struct image* img) {
    if (img) {
        free(img->data);
        img->data = NULL;
    }
}
