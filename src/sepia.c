//
// Created by ���ᨬ on 31.12.2023.
//

#include "sepia.h"

extern void sepia_asm(struct pixel* px);

static const float kf[3][3] = {
        { .272f, .543f, .131f },
        {.349f, .686f, .168f},
        { .393f, .769f, .189f}
};

static uint8_t check(int r, int g, int b, float r_f, float g_f, float b_f) {
    int new = (r * r_f) + (g * g_f) + (b * b_f);

    if (new > 255) return 255;
    return new;
}

void sepia_c(struct pixel* pixel) {
    uint8_t new_b = check(pixel->r, pixel->g, pixel->b, kf[0][0], kf[0][1], kf[0][2]);
    uint8_t new_g = check(pixel->r, pixel->g, pixel->b, kf[1][0], kf[1][1], kf[1][2]);
    uint8_t new_r = check(pixel->r, pixel->g, pixel->b, kf[2][0], kf[2][1], kf[2][2]);

    pixel->r = new_r;
    pixel->g = new_g;
    pixel->b = new_b;
}

void sepia_filter_c(struct image* image) {
    if (!image) return;

    for(uint64_t i = 0; i < image->height; i++) {
        for(uint64_t j = 0; j < image->width; j++) {
            sepia_c(image->data + (i * image->width + j));
        }
    }

}

void sepia_filter_asm(struct image* image) {
    if (!image) return;

    for(uint64_t i = 0; i < image->height; i++) {
        for(uint64_t j = 0; j < image->width; j++) {
            sepia_asm(image->data + (i * image->width + j));
        }
    }
}