//
// Created by ���ᨬ on 31.12.2023.
//

#include "image.h"
#include "bmp_format.h"
#include "sepia.h"
#include "util.h"
#include "time.h"

int main( int argc, char** argv ) {
    FILE* f_in;
    FILE* f_out;
    struct image img_c;
    enum read_status rs;
    enum write_status ws;
    if (argc < 4) {
        fprintf(stderr, "Wrong number of input arguments\nExample: ./prog.exe file.bmp file2.bmp 90");
        return -1;
    }

    f_in = fopen(argv[1], "rb");
    if (!f_in) {
        fprintf(stderr, "Error with opening the reading-file!");
        return -1;
    }

    rs = from_bmp(f_in, &img_c);
    struct image img_asm = copy_image(&img_c);
    if (fclose(f_in) == EOF) {
        fprintf(stderr, "Error with closing the file!");
        delete_image(&img_c);
        return -1;
    }

    print_read_status(rs);
    print_newline();

    clock_t start_c = clock();
    sepia_filter_c(&img_c);
    clock_t end_c = clock();

    double c_time = ((double) (end_c - start_c)) / CLOCKS_PER_SEC;

    clock_t start_asm = clock();
    sepia_filter_asm(&img_asm);
    clock_t end_asm = clock();

    double asm_time = ((double ) (end_asm - start_asm)) / CLOCKS_PER_SEC;

    f_out = fopen(argv[2], "wb");
    if(!f_out) {
        fprintf(stderr, "Error with opening the writing-file!");
        delete_image(&img_c);
        return -1;
    }

    ws = to_bmp(f_out, &img_c);
    delete_image(&img_c);
    print_write_status(ws);
    print_newline();
    printf("Time on C: %f sec\n", c_time);

    f_out = fopen(argv[3], "wb");
    if(!f_out) {
        fprintf(stderr, "Error with opening the writing-file!");
        delete_image(&img_asm);
        return -1;
    }

    ws = to_bmp(f_out, &img_asm);
    delete_image(&img_asm);
    print_write_status(ws);
    print_newline();
    printf("Time on ASM: %f sec\n", asm_time);

    return 0;
}
