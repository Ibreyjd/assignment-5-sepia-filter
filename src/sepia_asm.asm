%macro load_px_to_xmm 2
    movzx eax, byte [%1]        ;
    cvtsi2ss %2, eax            ; ����㧪� � �८�ࠧ������ �� float � ����㧪� � ����� xmm
    shufps %2, %2, 0            ;
%endmacro

section .rodata

b_kf: dd 0.131, 0.168, 0.189, 0     ;
g_kf: dd 0.534, 0.686, 0.769, 0     ; �����業�� ��� b, g, r
r_kf: dd 0.272, 0.349, 0.393, 0     ;

section .text
global sepia_asm

sepia_asm:
    mov r8, [rdi + 3]

    load_px_to_xmm rdi, xmm0        ; ��७�ᨬ ��� b � xmm
    movups xmm1, [b_kf]             ;
    mulps xmm0, xmm1                ; 㬭����� ��� b �� ����樥��� � ����砥� [ b1, b2, b3, 0]

    load_px_to_xmm rdi + 1, xmm2    ; ��७�ᨬ �� g � xmm
    movups xmm3, [g_kf]             ;
    mulps xmm2, xmm3                ; --\\-- ����砥� [g1, g2, g3, 0]

    load_px_to_xmm rdi + 2, xmm4    ; ��७�ᨬ ��� r � xmm
    movups xmm5, [r_kf]             ;
    mulps xmm4, xmm5                ; --\\-- ����砥� [r1, r2, r3, 0]

    addps xmm0, xmm2
    addps xmm0, xmm4                ; = [b1 + g1 + r1, b2 + g2 + r2, b3 + g3 + r3]

    cvtps2dq xmm0, xmm0             ; � int32
    packusdw xmm0, xmm0             ; � int16
    packuswb xmm0, xmm0             ; � int8

    movd [rdi], xmm0
    mov [rdi + 3], r8
    ret