//
// Created by ���ᨬ on 31.12.2023.
//

#ifndef ASSIGNMENT_5_SEPIA_FILTER_SEPIA_H
#define ASSIGNMENT_5_SEPIA_FILTER_SEPIA_H

#include "image.h"

void sepia_filter_c(struct image* image);
void sepia_filter_asm(struct image* image);
#endif //ASSIGNMENT_5_SEPIA_FILTER_SEPIA_H
