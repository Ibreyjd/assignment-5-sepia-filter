//
// Created by ���ᨬ on 31.12.2023.
//
#ifndef ASSIGNMENT_5_SEPIA_FILTER_IMAGE_H
#define ASSIGNMENT_5_SEPIA_FILTER_IMAGE_H

#include <inttypes.h>
#include <stdlib.h>

#pragma pack(push, 1)

struct pixel { uint8_t b, g, r; };

#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(uint64_t const width, uint64_t const height);
struct image copy_image(struct image * const image);
void delete_image(struct image* img);

#endif //ASSIGNMENT_5_SEPIA_FILTER_IMAGE_H
