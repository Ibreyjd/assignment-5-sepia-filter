CC=gcc
INC_DIR=include
C_FLAGS=-I $(INC_DIR) -Wall -Werror
ASM=nasm
ASM_FLAGS=-g -f elf64
OBJ_DIR=main
OUT_FILE=main/sepia_prog
SRC_DIR=src

OBJ_FILES = $(patsubst $(SRC_DIR)/%.c, $(OBJ_DIR)/%.o, $(wildcard $(SRC_DIR)/*.c))
OBJ_FILES+= $(patsubst $(SRC_DIR)/%.asm, $(OBJ_DIR)/%.asm.o, $(wildcard $(SRC_DIR)/*.asm))

.PHONY: build clean run

build: $(OUT_FILE);

$(OUT_FILE): $(OBJ_FILES)
	$(CC) -o $@ $(OBJ_FILES) -no-pie

$(OBJ_DIR)/%.asm.o: $(SRC_DIR)/%.asm
	mkdir -p $(OBJ_DIR)
	$(ASM) $(ASM_FLAGS) -o $@ $<

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	mkdir -p $(OBJ_DIR)
	$(CC) $(C_FLAGS) -c -o $@ $<

clean:
	rm -rf $(OBJ_DIR)

run:
	$(OUT_FILE) input.bmp out_c.bmp out_asm.bmp